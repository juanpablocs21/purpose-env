var gulp = require("gulp");
var express = require("express");
var jade = require("jade");
var phpjade = require("phpjade");
var jip = require("jade-interpolate-plus");
var j4p = require("jade-for-php");
var gulpjade = require("gulp-jade");
var rename = require("gulp-rename");

//getting false data
var dataFront = require("./data/index");

var jadeFiles = {
    inputFolderName: "./jade/",
    input: [ "!jade/_*.jade", "jade/*.jade" ],
    output: "../backend/"
};

//paths
//for front
gulp.task("server", function(){
    var app = express();
    app.set("view engine", 'jade');
    app.set("views", "./");

    jade.filters.php = function(block){
        //var onlyvars = block.replace(block, data.foobar);
        return "";
    };

    app.get("/", function(req, res){
      res.render(jadeFiles.inputFolderName + "index.jade", dataFront);
    });

    app.listen("8085");
});

//for testing compiling
gulp.task("compile:test", function(cb){
    jip.init(jade);
    return gulp.src(jadeFiles.input)
        .pipe(gulpjade({
            jade: jade,
            pretty: true,
            locals: dataFront,
            //debug: true
        }))
        .pipe(rename({
            extname: ".php"
        }))
        .pipe(gulp.dest(jadeFiles.output));
});

//testing plugin
gulp.task("compile:jip", function(cb){
    jip.init(jade);
    return gulp.src(jadeFiles.input)
        .pipe(gulpjade({
            jade: jade,
            pretty: true,
            locals: dataFront,
            //debug: true
        }))
        .pipe(rename({
            extname: ".php"
        }))
        .pipe(gulp.dest(jadeFiles.output));
});

//for back
//common/current task
gulp.task("compile", function(cb){
    jip.init(jade);
    j4p.init(jade);
    return gulp.src(jadeFiles.input)
        .pipe(gulpjade({
            pretty: true
        }))
        .pipe(rename({
            extname: ".php"
        }))
        .pipe(gulp.dest(jadeFiles.output));
});
